(function () {
    'use strict';

    /* App Module */

    var itemsApp = angular.module('itemsApp', [
      'ngRoute',
      'itemsControllers'
    ]);

    itemsApp.config(['$routeProvider',
      function($routeProvider) {
        $routeProvider.
          when('/items', {
            templateUrl: 'index.php/welcome/poll',
            controller: 'ItemListCtrl'
          }).
          when('/items/:itemId', {
            templateUrl: 'index.php/welcome/poll/view',
            controller: 'ItemDetailCtrl'
          }).	
			when('/admin/edit/:itemId', {
            templateUrl: 'index.php/welcome/admin/edit',
            controller: 'AdminEditPoll'
          }).		  
		  when('/admin/new', {
            templateUrl: 'index.php/welcome/admin/new',
            controller: 'AdminNewPoll'
          }).
          when('/admin/:itemId', {
            templateUrl: 'index.php/welcome/admin/view',
            controller: 'AdminDetailCtrl'
          }).		  

            when('/admin/', {
            templateUrl: 'index.php/welcome/admin',
            controller: 'AdminListCtrl'
          }).when('/about/', {
            templateUrl: 'index.php/welcome/about',
            
          }).
          otherwise({
            redirectTo: '/items'
          });
      }]);
}())
