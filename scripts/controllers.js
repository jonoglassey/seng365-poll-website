(function() {
    'use strict';

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-bottom-full-width",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "1000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }



    /* Controllers */

    // Global "database"
    var itemList = [];

    var itemsControllers = angular.module('itemsControllers', [
        'ngAnimate']);

    itemsControllers.controller('AdminListCtrl', ['$scope', '$http',
        function($scope, $http) {

            $scope.deletePoll = function(id) {
                console.log(id);
                $http.delete('index.php/services/polls/' + id).success($scope.loadPolls);
            }
            $scope.loadPolls = function() {
                $http.get('index.php/services/polls/').success(
                        function(data) {
                            $scope.items = data;
                        });
            }

            $scope.loadPolls();
        }]);

    itemsControllers.controller('AdminNewPoll', ['$scope', '$http', '$location',
        function($scope, $http, $location) {

            $scope.poll = {
                title: '',
                question: '',
                options: []
            }

            $scope.submit = function() {
                //console.out($scope.poll);
                $http.post('index.php/services/polls/', $scope.poll).success(
                        function(data, status, headers, config) {
                            $location.path('/admin');
                            console.log('New Poll Created');
                        }
                )
            };
        }]);

    itemsControllers.controller('AdminEditPoll', ['$scope', '$http', '$routeParams',
        function($scope, $http, $routeParams) {
            var itemId = $routeParams.itemId;
            $http.get('index.php/services/polls/' + itemId).success(
                    function(data) {
                        $scope.poll = {
                            title: data.title,
                            question: data.question,
                            options: data.responses
                        }

                        console.log($scope.poll);
                    }
            )

            $scope.submit = function() {
                //console.out($scope.poll);
                $http.put('index.php/services/polls/' + itemId, $scope.poll).success(
                        function() {
                            console.log('Poll Edited');
                        }
                ).error(
                        function(data) {
                            console.log(data);
                        }
                )
            };
        }]);

    itemsControllers.controller('AdminDetailCtrl', ['$scope', '$http', '$routeParams', '$route', '$sce',
        function($scope, $http, $routeParams, $route) {
            var itemId = $routeParams.itemId;
            var graphResults = [];
            var displayGraph = false;
            
            $('#graph').hide();
            
            $scope.error = '';
            $scope.loadData = function() {
                $http.get('index.php/services/polls/' + itemId).success(
                        function(data) {
                            $scope.item = data;
                            //console.log($scope.item.responses);
                        }).error(
                        function(data) {
                            $scope.isError = true;
                        }
                ).then(function() {
                    $http.get('index.php/services/votes/' + itemId).success(
                            function(data) {
                                var i = 0;

                                $scope.options = data.options;
                                $scope.votes = data.votes;
                                
                                //Populate array for Graph
                                for (i = 0; i < Object.keys($scope.votes).length; i++) {
                                    if($scope.votes[i] > 0) {
                                        graphResults.push([$scope.options[i], $scope.votes[i]]);
                                        displayGraph = true;
                                    }
                                }

                                
                                if (displayGraph) {
                                    $('#graph').show();
                                }
                                $('#graph').highcharts({
                                    chart: {
                                        plotBackgroundColor: null,
                                        plotBorderWidth: null,
                                        plotShadow: false
                                    },
                                    title: {
                                        text: "",
                                    },
                                    tooltip: {
                                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                    },
                                    plotOptions: {
                                        pie: {
                                            allowPointSelect: true,
                                            cursor: 'pointer',
                                            dataLabels: {
                                                enabled: false
                                            },
                                            showInLegend: true
                                        }
                                    },
                                    series: [{
                                            type: 'pie',
                                            name: 'Browser share',
                                            data: graphResults
                                        }]
                                });

                            }
                    )
                })
            };

            $scope.resetPoll = function() {
                $http.delete('index.php/services/votes/' + itemId).success(
                        function(data) {
                            console.log('Delete Successful');
                            toastr.success("The list has been cleared", "Success");
                            graphResults = [];
                            displayGraph = false;
                            $('#graph').hide();
                            $scope.loadData();
                            
                        }
                )
            }

            $scope.loadData();




        }]);

    itemsControllers.controller('ItemListCtrl', ['$scope', '$http',
        function($scope, $http) {
            $http.get('index.php/services/polls/').success(
                    function(data) {
                        //console.log(response);
                        $scope.items = data;
                    });
            //console.log($scope.items);
            //$scope.items = $scope.itemList;
            //$scope.author = 'Angus McGurkinshaw';
        }]);

    itemsControllers.controller('ItemDetailCtrl', ['$scope', '$http', '$routeParams', '$location',
        function($scope, $http, $routeParams, $location) {
            var itemId = $routeParams.itemId;
            $scope.sendChoice = function() {
                console.log($scope.choice);
                if($scope.choice !== undefined) {
                $http.post('index.php/services/votes/' + itemId + '/' + $scope.choice).success
                        (
                                function() {
                                    toastr.success("Your vote has been cast!", "Success");
                                    $location.path('/items');
                                })};
            };
            $http.get('index.php/services/polls/' + itemId).success(
                    function(data) {
                        $scope.item = data;
                        console.log($scope.item.responses);
                    }).error(
                    function() {
                        $scope.isError = true;
                    }
            )




        }]);


}())
