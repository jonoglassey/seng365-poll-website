<?php

class Vote extends CI_Model {
    public $id;
    public $pollid;
    public $polloption;
    public $ip;
    
    public function __construct() {
        $this->load->database();
        $this->tableName = "polls";
        $this->load->model("poll");
    }

    public function getVotes($pollid) {
        $this->db->select('*');
        $this->db->from('options');
        $this->db->where('options.pollid = ' . $pollid);
        $this->db->join('responses', 'options.pollid = responses.pollid AND options.order = responses.polloption', 'left');
        
        
        $query = $this->db->get();
        $result = $query->result();
        //var_dump($result);
        //exit();
        $votearray = array();
        $optionarray = array();
        
        if ($query->num_rows() == 0) {
            throw new Exception("No Votes for Poll of id $pollid or no poll exists");
        }
        
        
        
        foreach($result as $row) {
            
            $votearray[$row->order] = 0;
            $optionarray[$row->order] = $row->answer;
        }
        
        
        foreach ($result as $row) {
            //TODO: Slightly hacky to make sure row contains an answer. Fix at some point
            
            if(isset($row->polloption)) {
                
                $votearray[$row->polloption]++;
            }
        }
        

        $votes = array(
            'votes' => $votearray,
            'options' => $optionarray,
        );
       
        
        return $votes;
    }
    
    public function insertVote($pollId, $vote) {
        
        if(!$this->poll->pollExists($pollId)) {
            throw new Exception('Poll does not exist');
        }
        $vote = array(
            'pollid' => $pollId,
            'polloption' => $vote,
            'ip' => $_SERVER['REMOTE_ADDR']
        );

        $this->db->insert('responses', $vote);
        //$this->response("Success");

        if ($this->db->affected_rows() === 0) {
            throw new Exception('Failed to Insert');
        }
        
       return true;
    }
}
