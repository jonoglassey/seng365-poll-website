<?php

class Poll extends CI_Model {

    private $tableName;
    public $id;
    public $title;
    public $question;
    public $responses;

    public function __construct() {
        $this->load->database();
        $this->tableName = "polls";
    }

    public function read($id) {
        $this->db->select('*');
        $this->db->from($this->tableName);
        $this->db->where('polls.id', $id);
        $this->db->join('options', 'options.pollid = polls.id', 'left');
        $this->db->order_by('order');

        $query = $this->db->get();

        if ($query->num_rows === 0) {
            //exit("exception");
            throw new Exception("Poll of ID $id not found in Database");
        }

        $poll = new Poll();
        $result = $query->result();
        //exit(var_dump($query->result(0)));
        $row = $result[0];

        $options = $this->getOptions($row->pollid, $result);

        $poll->load($row, $options);

        return $poll;
    }

    public function getOptions($id, $result) {
        $questions = array();
        foreach ($result as $row) {
            $option = array();
            if ($row->pollid === $id) {
                $questions["$row->order"] = $row->answer;
            }
        }

        //exit(var_dump($result));
        return $questions;
    }

    public function getAll() {
        $polls = array();
        $this->db->select('*');
        $this->db->from($this->tableName);

        $query = $this->db->get();
        $result = $query->result();
        //exit(var_dump($query->result()));

        if ($query->num_rows() <= 0) {
            throw new Exception("No rows present in Database");
        }

        foreach ($result as $row) {
            //echo $i;
            $polls[] = $this->read($row->id);
        }
        //exit($query->num_rows());
        //exit(var_dump($polls));
        return $polls;
    }

    private function load($row, $options) {
        $this->id = $row->pollid;
        $this->title = htmlspecialchars($row->title);
        $this->question = htmlspecialchars($row->question);
        $this->responses = array();

        foreach ($options as $key => $option) {
            $this->responses[$key] = htmlspecialchars($option);
        }
    }

    public function pollExists($id) {
        $query = $this->db->get_where('polls', array('id' => $id));
        if ($query->num_rows() === 0) {
            return false;
        }

        return true;
    }

    public function editPoll($id, $title, $question, $options) {
        if (sizeof($options) < 2) {
            throw new Exception('A poll requires two or more options');
        }

        if (empty($title)) {
            throw new Exception('A poll requires a title');
        }

        if (empty($question)) {
            throw new Exception('A poll requires a question');
        }

        $poll = $this->read($id);

        if ($poll->title !== $title || $poll->question !== $question) {
            $editedPoll = array(
                'title' => $title,
                'question' => $question
            );

            $this->db->where('id', $id);
            $this->db->update('polls', $editedPoll);
        }

        if (array_diff($options, $poll->responses))
            $this->db->delete('options', array('pollid' => $id));
            $this->db->delete('responses', array('pollid' => $id));
            
            $i = 0;
            foreach ($options as $option) {

                $newOption = array(
                    'answer' => $option,
                    'pollid' => $id,
                    'order' => $i
                );

                $this->db->insert('options', $newOption);
            }
        $i++;
    }

    /**
     * Creates a Poll
     * @param type $title
     * @param type $question
     * @param type $option
     * @return type
     * @throws Exception
     */
    public function createPoll($title, $question, $option) {

        if (sizeof($option) < 2) {
            throw new Exception('A poll requires two or more options');
        }

        if (empty($title)) {
            throw new Exception('A poll requires a title');
        }

        if (empty($question)) {
            throw new Exception('A poll requires a question');
        }

        $poll = array(
            'title' => $title,
            'question' => $question
        );

        $this->db->insert('polls', $poll);

        $this->db->select_max('id');
        $query = $this->db->get('polls');
        $result = $query->result();
        $maxId = $result[0]->id;

        $i = 0;
        foreach ($option as $option) {
            $option = array(
                'pollid' => $maxId,
                'answer' => $option,
                'order' => $i
            );

            $this->db->insert('options', $option);
            $i++;
        }
        
        return $maxId;
    }

    public function deletePoll($id) {
        $this->db->delete('polls', array('id' => $id));

        if ($this->db->affected_rows() == 0) {
            throw new Exception("Poll does not exist");
        }

        $this->db->delete('responses', array('pollid' => $id));
        $this->db->delete('options', array('pollid' => $id));
    }

}