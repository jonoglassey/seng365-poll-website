<h1>Polls</h1>
<h3>Please Select a Poll to Participate in</h3>
<ul class='nopadding'>
<li class='poll-list-item' ng-repeat="item in items">
  <a href="#/items/{{item.id}}">
	<button type="button" class="btn btn-default btn-lg poll-button">
	  <span class="glyphicon glyphicon-pencil"></span> {{item.title}}
	</button>	
  </a>
</li>
</ul>
