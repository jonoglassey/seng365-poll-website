<div ng-hide='isError'>
	<h1>{{item.title}}</span></h1>
	<h3>Question: {{ item.question }}</h3>
	<table class='table'>
		<th>Response</th>
		<th>Votes</th>
		<tr ng-repeat="(key, vote) in votes">
			<td ng-bind='options[key]'></td>
			<td ng-bind='vote'></td>
		</tr>
	</table>

	<div id="graph" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
	<button ng-click='resetPoll()'>Reset</button>
</div>
  <div ng-show='isError'>
		This poll does not exist
  </div>




