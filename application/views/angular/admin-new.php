
<form class='col-xs-12'>
	<h1>Create New Poll</h1>
	<input ng-model='poll.title' type='text' name='pollTitle' id='pollTitle' class='form-control' placeholder='Poll Title' required>
	<label for='pollQuestion'>Question</label>
	<input ng-model='poll.question' type = 'text' name = 'pollQuestion' id='pollQuestion' class='form-control' placeholder='Poll Question' required>
	<label for='option0'>Option 1</label>
	<input ng-model='poll.options[0]' type='text' name='option0' id='option0' class='form-control' required>
	<label for='option1'>Option 2</label>
	<input ng-model='poll.options[1]' type = 'text' name = 'option1' id='option1' class='form-control' required>
	<label for='option2'>Option 3</label>
	<input ng-model='poll.options[2]' type='text' name='option2' id='option2' class='form-control'>
	<label for='option3'>Option 4</label>
	<input ng-model='poll.options[3]' type = 'text' name = 'option3' id='option3' class='form-control'>
	<label for='option3'>Option 5</label>
	<input ng-model='poll.options[4]' type = 'text' name = 'option4' id='option4' class='form-control'>
	<input ng-click='submit()' class="newPollButton btn btn-primary btn-lg btn-block" type='submit' name='submit'>
</form>

