<div class='col-xs-12 nopadding question'>


<div class='aboutPage col-xs-6'>
	<h4>Jonathan Glassey : Oct 13 2014</h4>
	
	
	<h1>About Page</h1>
	<h3>What is this site?</h3>
	This is a site where users can vote on polls created in Angular
	
	<h3>Voting</h3>
	To Vote please go to the URL <a href="http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/#/about/">http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/#/about/</a>
	and select one of the polls displayed on the page, the poll will then be displayed to you, click the option you wish to vote for an then click the submit button
	
	<h2>Admin Control</h3>
	<p style='color:red'>* Please Note that there is no user functionality implemented for this site, although these are intented for admin use anyone can currently access them</p>
	
	To access the Admin CP for the website please go to <a href="http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/#/admin/">http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/#/admin/</a>
	Here you will be presented with a list of polls the avaliable to the user.
	
	<h3>Viewing Poll Votes</h3>
	On the Admin Page, clicking on the name of a poll will send you to a page where you can see
	information on the poll. That is a break down on the votes seen on a table and a pie graph displayed below.
	You can reset the poll votes by clicking the reset button 
	
	<h3>Making New Polls</h3>
	You can create polls on this website to create a poll click on the create new button on the admin page
	this will take you to a page for creating a new poll. Here you can enter a title, question and up to four
	responses to the poll. Please not that you must have at least two responses to a poll to submit it.
	
	<h3>Deleting Polls</h3>
	By pressing the delete button beside a poll on the Admin Poll View you can delete the poll, please be aware
	this action is not reversable
	
	<h2>Extra Notes</h2>
	
	<h3>Responsive Design</h3>
	As this website was built using the Bootstrap frame work the website can easily be used on a mobile web browser
	just load up the site at the normal URL to be displayed the mobile version of the site.

	
</div>

<div class='aboutservices col-xs-6'>
	<h2>Services</h2>
	The following Services have been implemented
	
	<h2>Polls</h2>
	<h3>GET</h3>
	<a href="http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/polls/">http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/polls/</a>
	<table class='table'>
		<th>Response Header</th>
		<th>Output</th>
		<tr>
			<td>200 OK</td>
			<td>JSON containing information on all polls in database including id, title and all avaliable options.</td>
		</tr>
		<tr>
			<td>404 Not Found</td>
			<td>A message containing the reason why the request failed from the thrown Exception that as caught</td>
		</tr>
	</table>
	
	<a href="http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/polls/{pollId}">http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/polls/</a>
	<table class='table'>
		<th>Response Header</th>
		<th>Output</th>
		<tr>
			<td>200 OK</td>
			<td>JSON containing all information on the poll with the id in the URI from the database including id, title and all avaliable options.</td>
		</tr>
		<tr>
			<td>404 Not Found</td>
			<td>A message containing the reason why the request failed from the thrown Exception that as caught</td>
		</tr>
	</table>
	
	<h3>POST</h3>
	 http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/polls/
	 
	 <h4>POST Data</h4>
	 <table class='table'>
		<th>Key</th>
		<th>Data</th>
		<tr>
			<td>title</td>
			<td>Title of the Poll</td>
		</tr>
		<tr>
			<td>question</td>
			<td>Question of the Poll</td>
		</tr>
		<tr>
			<td>options</td>
			<td>0 or more Poll Options</td>
		</tr>
	</table>
	<h4>Responses</h4>
	<table class='table'>
		<th>Response Header</th>
		<th>Output</th>
		<tr>
			<td>201 Created & Location</td>
			<td>No Data Returned, URL of Poll's locatoin in the main applicatoin is sent back in the Location: Header. Poll is created</td>
		</tr>
		<tr>
			<td>404 Not Found</td>
			<td>The error of the raised exception will be returned. Poll will not be created. Reasons for failure include
				<ul>
					<li>No title was submitted</li>
					<li>No Question was submitted</li>
					<li>Less than two questions were submitted</li>
				</ul>
			</td>
		</tr>
	</table>
	
	<h3>DELETE</h3>
	http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/polls/{pollid}
	<h4>Responses</h4>
	<table class='table'>
		<th>Response Header</th>
		<th>Output</th>
		<tr>
			<td>200 OK</td>
			<td>Successfully Deleted Message Outputted, Poll is successfully dleted</td>
		</tr>
		<tr>
			<td>404 Not Found</td>
			<td>The error of the raised exception will be returned. Poll will not be deleted</td>
		</tr>
	</table>
	
	<h2>Votes</h2>
	<h3>POST</h3>
	http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/votes/
		<h4>Responses</h4>
	<table class='table'>
		<th>Response Header</th>
		<th>Output</th>
		<tr>
			<td>200 OK</td>
			<td>Message stating success, Vote is added to Database</td>
		</tr>
		<tr>
			<td>404 Not Found</td>
			<td>The error of the raised exception will be returned. Vote will not be added</td>
		</tr>
	</table>
	
	<h3>GET</h3>
	http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/votes/{pollid}
	<h4>Responses</h4>
	<table class='table'>
		<th>Response Header</th>
		<th>Output</th>
		<tr>
			<td>200 OK</td>
			<td>JSON Containing the number of votes for the poll of the passed in id {pollid}</td>
		</tr>
		<tr>
			<td>404 Not Found</td>
			<td>The error of the raised exception will be returned. As this is a result of no data being found for the poll message reflecting this will be outputted</td>
		</tr>
	</table>
	
	<h3>DELETE</h3>
	http://csse-studweb3.canterbury.ac.nz/~jsg40/polls/index.php/services/votes/{pollid}
	<h4>Responses</h4>
	<table class='table'>
		<th>Response Header</th>
		<th>Output</th>
		<tr>
			<td>200 OK</td>
			<td>"Success" is outputted, all votes for the datapase of {pollid} will be deleted</td>
		</tr>
		<tr>
			<td>404 Not Found</td>
			<td>Outputted if Poll is not a valid poll. A message reflecting this failure will be outputted</td>
		</tr>
	</table>
	
	
</div>

<div class='col-xs-12'>
	<h2>Notes</h2>
	
	<h3>Creation of Polls</h3>
	I have enforced the requirement of a title,  question and at least two responses and while there is a possiblity for a poll
	to have a potentially unlimied amount of options, for simplicity sake I have limited it to 5 options.
	
	<h2>Issues of Note</h2>
	<h3>Soft Resource Errors</h3>
	If a user tries to browse to a URI that does not contain a valid resource they will be displayed with an error
	however this page does not give a 404 error, in the future I plan to correct this issues
	
	<h3>Editing</h3>
	Please understand that while there is code for an Edit Feature and the put service in the services controller
	is implemented I abandonned this feature during development due to how time-consuming it was becoming.
	
	<h2>Code from Outside</h2>
	This website makes use of the Bootstrap framework for a large amout of the styling and 
	graphs are made using <a href="http://www.highcharts.com/"> High Charts</a>
</div>

</div>
</div>




