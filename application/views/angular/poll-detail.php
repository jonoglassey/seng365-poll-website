<div class='col-xs-12 nopadding question' ng-hide='isError'>
	<h1 class='col-xs-12'>{{item.title}}</span></h1>
	<h3 class='col-xs-12'>Question: {{ item.question }}</h3>
	<form>
			<div ng-repeat="(order, response) in item.responses" class='toggleButton col-xs-12'>
				<label class='col-xs-12 nopadding'>
				<input type="radio" name='choice' value={{order}} ng-model='$parent.choice'/>
					<span>{{response}}</span>
				</label>	
			</div>
			
			<input type='submit' class="newPollButton btn btn-primary btn-lg btn-block col-xs-12" ng-click=sendChoice() >
	</form>
</div>

  <div ng-show='isError'>
		This poll does not exist
  </div>



