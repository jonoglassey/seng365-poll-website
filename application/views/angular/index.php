<!doctype html>
<!-- A rudimentary 2-view Angular app, for SENG365 2014. RJL. -->
<html lang="en" ng-app="itemsApp">
    <head>
        <meta charset="utf-8">
        <title>Items</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url('styles/style.css'); ?>">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.js"></script>

        <script src="<?php echo base_url('scripts/angular.js'); ?>"></script>
        <script src="<?php echo base_url('scripts/angular-route.js') ?>"></script>
        <script src="<?php echo base_url('scripts/app.js') ?>"></script>
        <script src="<?php echo base_url('scripts/controllers.js') ?>"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-animate.js"></script>
        <script src="http://code.highcharts.com/highcharts.js"></script>
		<script src="http://code.highcharts.com/modules/exporting.js"></script>

        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>

        <div class="navbar navbar-inverse" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Poll Site</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#/items/">Polls</a></li>
                        <li><a href="#/admin/">Admin</a></li>
                        <li><a href="#/about/">About</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <div ng-view class='container'>


    </body>
</html>
