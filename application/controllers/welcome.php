<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->helper('url');
        $this->load->view('angular/index');
    }

    function poll($page = null) {
        if ($page === null) {
            $this->load->view('angular/poll-list');
        } elseif ($page === 'view') {
            $this->load->view('angular/poll-detail');
        }
    }

    function admin($page = null) {
        if ($page == null) {
            $this->load->view('angular/admin-list');
        } else if ($page === 'view') {
            $this->load->view('angular/admin-detail');
        } else if ($page === 'new') {
            $this->load->view('angular/admin-new');
        }
    }
    
    function about() {
        $this->load->view('angular/about');
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */