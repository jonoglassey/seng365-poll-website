<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . "/libraries/REST_Controller.php");

class Services extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('poll');
        $this->load->model('vote');
        $this->load->database();
    }

    function polls_get($id = null) {
        $this->output->set_content_type('application/json');
        //var_dump($this->get('id'));
        if ($id === null) {
            try {
                $polls = $this->poll->getAll();
                $this->output->set_header("HTTP/1.1 200 OK");
                $this->output->set_output(json_encode($polls), JSON_FORCE_OBJECT);
                
            } catch (Exception $e) {
                $this->output->set_header("HTTP/1.0 404 Not Found");
                $this->output->set_output($e->getMessage());
            }
        } else {
            try {
                $poll = $this->poll->read($id);
                $this->output->set_header("HTTP/1.1 200 OK");
                $this->output->set_output(json_encode($poll), JSON_FORCE_OBJECT);
            } catch (Exception $e) {
                $this->output->set_header("HTTP/1.0 404 Not Found");
                $this->output->set_output($e->getMessage());
            }
        }
    }

    function polls_post() {
        $pollTitle = $this->post('title');
        $pollQuestion = $this->post('question');
        $options = $this->post('options');
        
        try {
            $pollId = $this->poll->createPoll($pollTitle, $pollQuestion, $options);
            $this->output->set_header("HTTP/1.1 201 Created");
            
            $host  = $_SERVER['HTTP_HOST'];
            
            $this->output->set_header("Location: $host/~jsg40/polls/#/items/$pollId");
        }
        
        catch(Exception $e) {
           $this->output->set_header("HTTP/1.0 404 Not Found");
           $this->output->set_output($e->getMessage()); 
        }
        
    }

    function polls_put($id = null) {
        //TODO: SQL Special Escaping;
        $pollTitle = $this->put('title');
        $pollQuestion = $this->put('question');
        $options = $this->put('options');
        
        try {
            $this->poll->editPoll($id, $pollTitle, $pollQuestion, $options);
        }
        catch (Exception $e) {
           $this->output->set_header("HTTP/1.0 404 Not Found");
           $this->output->set_output($e->getMessage()); 
        }
    }

    function polls_delete($id = null) {
        
        try {
            $this->poll->deletePoll($id);
            $this->output->set_header("HTTP/1.1 200 OK");
            $this->output->set_output('Poll Successfully Deleted');
        }
        catch (Exception $e) {
            $this->output->set_header("HTTP/1.0 404 Not Found");
            $this->output->set_output($e->getMessage());
            
        }
    }

    function votes_post($pollId, $vote) {
        //TODO: Implement a check for pollId;
        try {
            $this->vote->insertVote($pollId, $vote);
            $this->output->set_header("HTTP/1.1 200 OK");
        } catch (Exception $e) {
            $this->output->set_header("HTTP/1.0 404 Not Found");
            $this->output->set_output($e->getMessage());
        }
    }

    function votes_get($pollid) {
        $this->output->set_content_type('application/json');

        try {
            $votes = $this->vote->getVotes($pollid);
            $this->output->set_header("HTTP/1.1 200 OK");
            $this->output->set_output(json_encode($votes, JSON_FORCE_OBJECT));
        } catch (Exception $e) {
            $this->output->set_header("HTTP/1.0 404 Not Found");
            $this->output->set_output($e->getMessage());
        }
    }

    function votes_delete($id) {
        $this->db->delete('responses', array('pollid' => $id));

        if ($this->db->affected_rows() > 0) {
            $this->output->set_header("HTTP/1.1 200 OK");
            $this->response("Success");
        } else {
            $this->output->set_header("HTTP/1.0 404 Not Found");
            $this->response($e->getMessage());
        }
    }

}